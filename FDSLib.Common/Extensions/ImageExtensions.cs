﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;

namespace FDSLib.Common.Extensions
{
    public static class ImageExtensions
    {

        /// <summary>
        /// Converts the current image object into a byte array.
        /// </summary>
        /// <returns></returns>
        public static byte[] ToByteArray(this Image image)
        {
            if (image == null)
            {
                return default(byte[]);
            }

            using (MemoryStream ms = new MemoryStream())
            {
                image.Save(ms, ImageFormat.Png);
                return ms.ToArray();
            }
        }

        /// <summary>
        /// Converts the current image object into a byte array.
        /// </summary>
        /// <param name="image">The current image object.</param>
        /// <param name="format">The format of the image.</param>
        /// <returns></returns>
        public static byte[] ToByteArray(this Image image, ImageFormat format)
        {
            if (image == null)
            {
                return default(byte[]);
            }

            using (MemoryStream ms = new MemoryStream())
            {
                image.Save(ms, format);
                return ms.ToArray();
            }
        }

    }
}
