﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using FDSLib.Common.Helpers;


namespace FDSLib.Common.Extensions
{
    public class XmlExtensions
    {
    }

    /// <summary>
    /// Xml helper methods
    /// </summary>
    public static class XElementExtensions
    {

        /// <summary>
        /// Returns the value of the supplied node name.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="element"></param>
        /// <param name="name">Name of xml node.</param>
        /// <returns></returns>
        public static T GetValue<T>(this XElement element, string name)
        {
            try
            {
                return ObjectHelper.ConvertToType<T>(element.Element(name).Value);
            }
            catch
            {
                return default(T);
            }
        }

        /// <summary>
        /// Returns the value of the supplied node name or the default value if the node is null.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="element"></param>
        /// <param name="name">Name of xml node.</param>
        /// <returns></returns>
        public static T GetValueOrDefault<T>(this XElement element, string name)
        {
            if (String.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }

            if (element.Element(name) == null)
            {
                return default(T);
            }

            try
            {
                return ObjectHelper.ConvertToType<T>(element.Element(name).Value);
            }
            catch
            {
                return default(T);
            }
        }

        /// <summary>
        /// Returns the value of the supplied node name or null if the node is null.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="element"></param>
        /// <param name="name">Name of xml node.</param>
        /// <returns></returns>
        public static T? GetValueOrNull<T>(this XElement element, string name) where T : struct
        {
            if (String.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }

            if (element.Element(name) == null)
            {
                return (T?)null;
            }

            try
            {
                return ObjectHelper.ConvertToType<T>(element.Element(name).Value);
            }
            catch
            {
                return null;
            }
        }

    }
}
