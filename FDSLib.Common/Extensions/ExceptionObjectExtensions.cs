﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FDSLib.Common.Extensions
{
    public static class ExceptionObjectExtensions
    {
        /// <summary>
        /// Returns the last exception within the chain of exceptions.
        /// </summary>
        /// <param name="ex">Exception object.</param>
        /// <returns></returns>
        public static Exception GetLastInnerException(this Exception ex)
        {
            if (ex == null)
            {
                throw new ArgumentNullException("ex");
            }

            while (ex.InnerException != null)
            {
                ex = ex.InnerException;
            }

            return ex;
        }

        /// <summary>
        /// Returns an enumerable list of all the inner exceptions that pertain to the exception object.
        /// </summary>
        /// <param name="ex">Exception object.</param>
        /// <returns></returns>
        public static List<Exception> GetInnerExceptionList(this Exception ex)
        {
            var list = new List<Exception>();

            if (ex == null)
            {
                throw new ArgumentNullException("ex");
            }

            while (ex.InnerException != null)
            {
                list.Add(ex);
                ex = ex.InnerException;
            }

            return list;
        }
    }
}
