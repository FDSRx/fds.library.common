﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Common.Extensions
{
    public static class EnumerableObjectExtensions
    {
        /// <summary>
        /// Returns the selected items in the collection to a delimited string.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection">IEnumerable list of items.</param>
        /// <param name="delimiter">Character to delimit by (e.g. ",").</param>
        /// <param name="property">Property to delimit.</param>
        /// <returns></returns>
        public static string ToDelimitedString<T>(this IEnumerable<T> collection, string delimiter, Func<T, string> property)
        {
            string delimitedString = String.Empty;

            try
            {
                return String.Join(delimiter, collection.Select(property).ToArray());
            }
            catch
            {
            }

            return delimitedString;
        }
    }
}
