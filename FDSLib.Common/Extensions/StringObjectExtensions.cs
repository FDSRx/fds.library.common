﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Globalization;

namespace FDSLib.Common.Extensions
{
    public static class StringObjectExtensions
    {

        /// <summary>
        /// To the pascal case.
        /// </summary>
        /// <param name="str">Accepts a string</param>
        /// <returns>Pascal cased items will be split at the uppper case letter, single words will just be captialized</returns>
        public static string Captialize(this string str)
        {
            if (string.IsNullOrWhiteSpace(str))
            {
                return string.Empty;
            }
            
            var parts = str.Trim().Split(' ').Select(p => string.Format("{0}{1}", p.Substring(0, 1).ToUpper(), p.Substring(1).ToLower()));
            return string.Join(" ", parts);
        }

        /// <summary>
        /// Uppercases the first character in the string.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string UppercaseFirstChar(this String str)
        {
            string outText;

            try
            {
                // Check for empty string.
                if (String.IsNullOrEmpty(str))
                {
                    return String.Empty;
                }

                // Return char and concat substring.
                outText = Char.ToUpper(str[0]) + str.Substring(1);
            }
            catch
            {
                outText = str;
            }

            return str = outText;
        }

        /// <summary>
        /// Indicates whether this instance is null or an System.String.Empty string.
        /// </summary>
        /// <param name="str">This instance.</param>
        /// <returns></returns>
        public static bool IsNullOrEmpty(this String str)
        {
            try
            {
                // Check for empty string.
                if (String.IsNullOrEmpty(str))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }

            return false;
        }

        /// <summary>
        /// Indicates whether this instance is null or contains white space.
        /// </summary>
        /// <param name="str">This instance.</param>
        /// <returns></returns>
        public static bool IsNullOrWhiteSpace(this String str)
        {
            try
            {
                // Check for a null or string with white spaces.
                if (String.IsNullOrWhiteSpace(str))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }

            return false;
        }
        
        /// <summary>
        /// Returns the string in proper case.  E.g. hELlO to Hello
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string ToTitleCase(this String str)
        {
            string outString;

            try
            {
                //requires the uppercase extesion to handle the string
                outString = !String.IsNullOrEmpty(str) ? CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str.ToLower()) : String.Empty;
            }
            catch
            {
                outString = str;
            }

            return str = outString;
        }

        /// <summary>
        /// Removes all non numeric characters from a string.  E.g. "36-789-AB-C-000" = "36789000"
        /// </summary>
        /// <param name="str">String to format.</param>
        /// <returns>String</returns>
        public static string RemoveNonNumericChars(this String str)
        {
            string result;

            try
            {
                result = Regex.Replace(str, "[^0-9]", "");
            }
            catch
            {
                result = str;
            }

            return str = result;
        }

        /// <summary>
        /// Returns this instance of System.String; no actual conversion is performed.
        /// </summary>
        /// <param name="obj">This object instance.</param>
        /// <returns>Returns the string or an empty string if the instance is null.</returns>
        public static string ToStringOrEmpty(this Object obj)
        {
            string outText = String.Empty;

            try
            {
                // Check for empty string.
                if (obj != null && !String.IsNullOrEmpty(obj.ToString()))
                {
                    outText = obj.ToString();
                }

            }
            catch
            {
                outText = String.Empty;
            }

            return outText;
        }

        /// <summary>
        /// Returns this instance of System.String; no actual conversion is performed.
        /// </summary>
        /// <param name="obj">This object instance.</param>
        /// <param name="alternateText">Specifies a string to return in place of a null string.</param>
        /// <returns>Returns the string or the specified empty text value in place of a null or empty string.</returns>
        public static string ToStringOrEmpty(this Object obj, string alternateText)
        {
            string outText;

            try
            {
                // Check for empty string.
                if (obj != null && !String.IsNullOrEmpty(obj.ToString()))
                {
                    outText = obj.ToString();
                }
                else
                {
                    outText = alternateText;
                }

            }
            catch
            {
                outText = String.Empty;
            }

            return outText;
        }

        /// <summary>
        /// Returns this instance of System.String; no actual conversion is performed. If the string is null, empty, or contains all white spaces,
        /// the alternate text is returned in its place.
        /// </summary>
        /// <param name="obj">This object instance.</param>
        /// <param name="alternateText">Alternate text that is to be returned in place of a null, empty, or all white space string object.</param>
        /// <returns></returns>
        public static string ToStringOrAlternate(this Object obj, string alternateText)
        {
            string result = null;

            try
            {
                if (obj != null && String.IsNullOrWhiteSpace(obj.ToString()))
                {
                    result = alternateText;
                }
                else
                {
                    result = obj != null ? obj.ToString() : alternateText;
                }
            }
            catch
            {
                result = alternateText;
            }

            return result;
        }

        /// <summary>
        /// Returns this instance of System.String; no actual conversion is performed. If the object is unable to return a string, then null is returned
        /// in its place.
        /// </summary>
        /// <param name="obj">This object instance.</param>
        /// <returns></returns>
        public static string ToStringOrNull(this Object obj)
        {
            string outText = String.Empty;

            try
            {
                outText = obj.ToString();

            }
            catch
            {
                outText = null;
            }

            return outText;
        }

        /// <summary>
        /// If the string is null or empty then return the alternate text in its place.
        /// </summary>
        /// <param name="str">String object.</param>
        /// <param name="altStr">Alternate string text.</param>
        /// <returns></returns>
        public static string IfNullOrEmpty(this String str, string altStr)
        {
            if (String.IsNullOrEmpty(str))
            {
                return str = altStr;
            }

            return str;
        }

        /// <summary>
        /// If the string is null or has a whitespace then return the alternate text in its place.
        /// </summary>
        /// <param name="str">String object.</param>
        /// <param name="altStr">Alternate string text.</param>
        /// <returns></returns>
        public static string IfNullOrWhiteSpace(this String str, string altStr)
        {
            if (String.IsNullOrWhiteSpace(str))
            {
                return str = altStr;
            }

            return str;
        }

        /// <summary>
        /// If the string is null then return the alternate text in its place.
        /// </summary>
        /// <param name="str">String object.</param>
        /// <param name="altStr">Alternate string text.</param>
        /// <returns></returns>
        public static string IfNull(this String str, string altStr)
        {
            if (str == null)
            {
                return str = altStr;
            }

            return str;
        }

        /// <summary>
        /// Surrounds the current System.String with double quotes (e.g. hello world = "hello world");
        /// </summary>
        /// <param name="text">The current System.String object.</param>
        /// <returns></returns>
        public static string ToDoubleQuotes(this String text)
        {
            return SurroundWith(text, "\"");
        }

        /// <summary>
        /// Surrounds the current System.String with single quotes (e.g. hello world = 'hello world');
        /// </summary>
        /// <param name="text">The current System.String object.</param>
        /// <returns></returns>
        public static string ToSingleQuotes(this String text)
        {
            return SurroundWith(text, "'");
        }

        /// <summary>
        /// Surrounds the current System.String with the provided surrounding text.
        /// </summary>
        /// <param name="text">The current System.String object.</param>
        /// <param name="surroundingText">The character or characters used to surround the text.</param>
        /// <returns></returns>
        public static string SurroundWith(this String text, string surroundingText)
        {
            return surroundingText + text + surroundingText;
        }

        /// <summary>
        /// Returns the characters from the left of the of System.String.
        /// </summary>
        /// <param name="text">The current System.String object.</param>
        /// <param name="length">Then number of characters to return.</param>
        /// <returns></returns>
        public static string Left(this String text, int length)
        {
            //we start at 0 since we want to get the characters starting from the
            //left and with the specified lenght and assign it to a variable
            string result = text.Substring(0, length);
            //return the result of the operation
            return result;
        }

        /// <summary>
        /// Returns the characters from the right of the of System.String.
        /// </summary>
        /// <param name="text">The current System.String object.</param>
        /// <param name="length">Then number of characters to return.</param>
        /// <returns></returns>
        public static string Right(this String text, int length)
        {
            //start at the index based on the lenght of the sting minus
            //the specified lenght and assign it a variable
            string result = text.Substring(text.Length - length, length);
            //return the result of the operation
            return result;
        }

        /// <summary>
        /// Returns the characters from the middle of the of System.String.
        /// </summary>
        /// <param name="text">The current System.String object.</param>
        /// <param name="startIndex">The index in which to start.</param>
        /// <param name="length">Then number of characters to return.</param>
        /// <returns></returns>
        public static string Mid(this String text, int startIndex, int length)
        {
            //start at the specified index in the string ang get N number of
            //characters depending on the lenght and assign it to a variable
            string result = text.Substring(startIndex, length);
            //return the result of the operation
            return result;
        }

        /// <summary>
        /// Returns the characters from the middle of the of System.String.
        /// </summary>
        /// <param name="text">The current System.String object.</param>
        /// <param name="startIndex">The index in which to start.</param>
        /// <returns></returns>
        public static string Mid(this String text, int startIndex)
        {
            //start at the specified index and return all characters after it
            //and assign it to a variable
            string result = text.Substring(startIndex);
            //return the result of the operation
            return result;
        }

        /// <summary>
        /// Indicates whether any of the supplied values are contained within the source item.
        /// </summary>
        /// <typeparam name="T">Object(Of T).</typeparam>
        /// <param name="source">Object(Of T).</param>
        /// <param name="list">List of items.</param>
        /// <returns></returns>
        public static bool IsAnyOf<T>(this T source, params T[] list)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }

            if (list == null)
            {
                throw new ArgumentNullException("list");
            }

            return list.Contains(source);
        }

        /// <summary>
        /// Indicates whether any of the values within the supplied list are contained within the source item.
        /// </summary>
        /// <typeparam name="T">Object(Of T).</typeparam>
        /// <param name="source">Object(Of T).</param>
        /// <param name="list">IEnumerable(Of T) object.</param>
        /// <returns></returns>
        public static bool IsAnyOf<T>(this T source, IEnumerable<T> list)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }

            if (list == null)
            {
                throw new ArgumentNullException("list");
            }

            return list.Contains(source);
        }

        /// <summary>
        /// Indicates whether any of the supplied values are not contained within the source item.
        /// </summary>
        /// <typeparam name="T">Object(Of T).</typeparam>
        /// <param name="source">Object(Of T).</param>
        /// <param name="list">List of items.</param>
        /// <returns></returns>
        public static bool IsNotAnyOf<T>(this T source, params T[] list)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }

            if (list == null)
            {
                throw new ArgumentNullException("list");
            }

            return !list.Contains(source);
        }

        /// <summary>
        /// Indicates whether any of the values within the supplied list are not contained within the source item.
        /// </summary>
        /// <typeparam name="T">Object(Of T).</typeparam>
        /// <param name="source">Object(Of T).</param>
        /// <param name="list">IEnumerable(Of T) object.</param>
        /// <returns></returns>
        public static bool IsNotAnyOf<T>(this T source, IEnumerable<T> list)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }

            if (list == null)
            {
                throw new ArgumentNullException("list");
            }

            return !list.Contains(source);
        }

        /// <summary>
        /// Returns a copy of a string converted to lowercase. If the string is unable to be converted to lowercase then null will be returned
        /// as its value.
        /// </summary>
        /// <param name="str">The current System.String object.</param>
        /// <returns></returns>
        public static string ToLowerOrDefault(this String str)
        {
            if (str == null)
            {
                return null;
            }

            return str.ToLower();
        }

        /// <summary>
        /// Converts the text value into phone number format.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns></returns>
        public static string ToPhoneNumber(this string str)
        {
            if (str.IsNullOrEmpty())
            {
                return String.Empty;
            }

            var matches = Regex.Matches(str, @"\d").Cast<Match>().Select(p => p.Value).ToArray();
            var numberOfDigits = matches.Length;

            if (numberOfDigits >= 7 && numberOfDigits <= 11)
            {
                //555-5555 (7 digit minimum) or 1-555-555-5555 (11 digit max)
                var last7Digits = matches.Skip(numberOfDigits - 7).Take(7).ToArray();
                var firstDigits = matches.Take(numberOfDigits - 7).ToArray();

                var phoneNumber = String.Format("{0}-{1}",
                    String.Join("", last7Digits.Take(3)),
                    String.Join("", last7Digits.Skip(3).Take(4)));

                if (firstDigits.Length > 2)
                {
                    phoneNumber = String.Format("({0}) {1}", String.Join("", firstDigits.Skip(firstDigits.Length - 3)), phoneNumber);
                }

                return phoneNumber;

            }
            return str;
        }

        /// <summary>
        /// True to indicate the string is only a string (i.e. no numbers or characters); otherwise, false to indicate it is not a string.
        /// </summary>
        /// <param name="str">The string value.</param>
        /// <returns></returns>
        public static bool IsString(this object str)
        {
            return str != null && Regex.IsMatch(str.ToString(), @"^[A-Za-z]+\Z");
        }

        /// <summary>
        /// Indicates whether the value is numeric or non-numeric.
        /// </summary>
        /// <param name="str">The System.String representation of the value to evaluate.</param>
        /// <returns></returns>
        public static bool IsNumeric(this string str)
        {
            bool isValid = true;

            double outVal;

            if (!double.TryParse(str, out outVal))
            {
                isValid = false;
            }

            return isValid;
        }

        /// <summary>
        /// Splits the string content on its designated line breaks (if applicable).
        /// </summary>
        /// <param name="input">The string content.</param>
        /// <returns></returns>
        public static IEnumerable<string> SplitLines(this string input)
        {
            if (input == null)
            {
                yield break;
            }

            using (System.IO.StringReader reader = new System.IO.StringReader(input))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    yield return line;
                }
            }
        }

    }
}