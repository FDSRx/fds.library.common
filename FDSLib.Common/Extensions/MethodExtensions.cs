﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace FDSLib.Common.Extensions
{
    public static class MethodExtensions
    {
        /// <summary>
        /// Executes the method as an Action.
        /// </summary>
        /// <typeparam name="A">The input method.</typeparam>
        /// <typeparam name="T">The output type of the func.</typeparam>
        /// <param name="f">The func method to execute.</param>
        /// <returns></returns>
        //[DebuggerStepThrough]
        public static Action<A> InvokeAsAction<A, T>(this Func<A, T> f)
        {
            return (a) => f(a);
        }


        /// <summary>
        /// Executes the method as a Func.
        /// </summary>
        /// <param name="a">The Action method to execute.</param>
        /// <returns></returns>
        //[DebuggerStepThrough]
        public static Func<bool> InvokeAsFunc(this Action a)
        {
            return () =>
                       {
                           a();
                           return true;
                       };
        }
    }

}
