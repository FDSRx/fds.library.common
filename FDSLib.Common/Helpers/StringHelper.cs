﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Text;

namespace FDSLib.Common.Helpers
{
    public class StringHelper
    {

        /// <summary>
        /// Determines if the value is a number
        /// </summary>
        /// <param name="value">String to inspect.</param>
        public static bool IsNumber(string value)
        {
            bool isNumber = false;
            decimal number = 0;

            try
            {
                if (Decimal.TryParse(value, out number))
                    isNumber = true;
                else
                    isNumber = false;
            }
            catch
            {
                isNumber = false;
            }

            return isNumber;
        }

        /// <summary>
        /// Returns the characters from the right of the string.
        /// </summary>
        /// <param name="str">String to read.</param>
        /// <param name="length">Number of characters to read from the right.</param>
        /// <returns></returns>
        public static string Right(string str, int length)
        {
            return str.Substring(length > str.Length ? 0 : str.Length - length);
        }

        /// <summary>
        /// Returns the characters from the left of the string.
        /// </summary>
        /// <param name="str">String to read.</param>
        /// <param name="length">Number of characters to read from the left.</param>
        /// <returns></returns>
        public static string Left(string str, int length)
        {
            return str.Substring(0, length);
        }

        /// <summary>
        /// Returns the selected items in the collection to a delimited string.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="delimiter"></param>
        /// <param name="convert"></param>
        /// <returns></returns>
        public static string ToDelimitedString<T>(IEnumerable<T> collection, string delimiter, Func<T, string> convert)
        {
            string delimitedString = String.Empty;

            try
            {
                return String.Join(delimiter, collection.Select(convert).ToArray());
            }
            catch
            {
            }

            return delimitedString;

        }

        /// <summary>
        /// Indicates if the string is a valid date.
        /// </summary>
        public static bool IsDate(string datetime)
        {
            bool isDate = true;

            DateTime outDate;

            if (!DateTime.TryParse(datetime, out outDate))
            {
                isDate = false;
            }

            return isDate;
        }

        /// <summary>
        /// Indicates if the string is a valid date.
        /// </summary>
        public static bool IsInteger(string value)
        {
            bool isInteger = true;

            int outInt;

            if (!int.TryParse(value, out outInt))
            {
                isInteger = false;
            }

            return isInteger;
        }

        /// <summary>
        /// Indicates whether the value is numeric or non-numeric.
        /// </summary>
        /// <param name="str">The System.String representation of the value to evaluate.</param>
        /// <returns></returns>
        public static bool IsNumeric(string str)
        {
            bool isValid = true;

            double outVal;

            if (!double.TryParse(str, out outVal))
            {
                isValid = false;
            }

            return isValid;
        }

        /// <summary>
        /// Indicates if the string is a valid phone number.
        /// </summary>
        public static bool IsPhoneNumber(string phoneNumber)
        {
            bool isPhoneNumber = true;

            isPhoneNumber = Regex.IsMatch(phoneNumber, @"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$");
            //isPhoneNumber = Regex.IsMatch(phoneNumber, @"\d{3}-\d{3}-\d{4}");
            //isPhoneNumber = Regex.IsMatch(phoneNumber, @"^(?:\(?)(\d{3})(?:[\).]?)(\d{3})(?:[-\.]?)(\d{4})(?!\d)");
            if (String.IsNullOrEmpty(phoneNumber))
                isPhoneNumber = false;

            return isPhoneNumber;
        }

        /// <summary>
        /// Indicates if the string is a valid email.
        /// </summary>
        public static bool IsEmail(string email)
        {
            bool isEmail = true;

            isEmail = Regex.IsMatch(email, @"^([\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+\.)*[\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+@((((([a-zA-Z0-9]{1}[a-zA-Z0-9\-]{0,62}[a-zA-Z0-9]{1})|[a-zA-Z])\.)+[a-zA-Z]{2,6})|(\d{1,3}\.){3}\d{1,3}(\:\d{1,5})?)$");

            if (String.IsNullOrEmpty(email))
                isEmail = false;

            return isEmail;
        }

        /// <summary>
        /// Returns string value of object.
        /// If unable to cast as a string an empty value will be returned.
        /// </summary>
        public static string GetStringValue(object value)
        {
            string outValue = String.Empty;

            try
            {
                if (value != null)
                    outValue = Convert.ToString(value);
            }
            catch { }

            return outValue;
        }

        /// <summary>
        /// Returns string value of object.
        /// The errorOutputValue can be used to specify the return string if the object cannot be cast as a string.
        /// </summary>
        public static string GetStringValue(object value, string errorOutputValue)
        {
            string outValue = String.Empty;

            try
            {
                if (value != null)
                    outValue = Convert.ToString(value);
                else
                    outValue = errorOutputValue;
            }
            catch
            {

            }

            return outValue;
        }

        /// <summary>
        /// Returns the object's instance of System.String; no actual conversion is performed.
        /// </summary>
        /// <param name="instance">Object to convert.</param>
        /// <returns>Returns the string or an empty string if the instance is null.</returns>
        public static string ToStringOrEmpty(object instance)
        {
            return ToStringOrEmpty(instance, String.Empty);
        }

        /// <summary>
        /// Returns the object's instance of System.String; no actual conversion is performed.
        /// </summary>
        /// <param name="instance">Object to convert.</param>
        /// <param name="emptyText">Specifies a string to return in place of a null string.</param>
        /// <returns>Returns the string or the specified empty text value in place of a null or empty string.</returns>
        public static string ToStringOrEmpty(object instance, string emptyText)
        {
            string outValue = String.Empty;

            try
            {
                if (instance != null)
                    outValue = Convert.ToString(instance);
                else
                    outValue = emptyText;
            }
            catch
            {

            }

            return outValue;
        }

        /// <summary>
        /// Determines if the object is a boolean.
        /// Valid boolean objects: true, false (string or bool), y, n, yes, no, 1, 0
        /// </summary>
        public static bool IsBoolean(object value)
        {
            bool result = false;

            bool? blnResult = value as bool?;

            if (blnResult != null)
                result = true; // could not determine boolean. It must be false right now


            string strResult = value.ToString();

            if (!String.IsNullOrEmpty(strResult))
            {
                switch (strResult.ToLower())
                {
                    case "true":
                    case "1":
                    case "yes":
                    case "y":
                    case "false":
                    case "0":
                    case "no":
                    case "n": result = true; break;
                    default:
                        result = false;
                        break;

                }
            }

            return result;

        }

        /// <summary>
        /// Converts object to true or false based on its value
        /// true, yes, y, 1 convert to true; false, no, n, 0 convert to false.  All other known strings convert to false.
        /// </summary>
        public static bool ToBoolean(object value)
        {
            bool result = false;

            bool? blnResult = blnResult = value as bool?;

            if (blnResult != null)
                return Convert.ToBoolean(blnResult);


            string strResult = value.ToString();

            if (!String.IsNullOrEmpty(strResult))
            {
                switch (strResult.ToLower())
                {
                    case "true":
                    case "1":
                    case "yes":
                    case "y": result = true; break;
                    case "false":
                    case "0":
                    case "no":
                    case "n": result = false; break;
                    default:
                        result = false;
                        break;

                }
            }

            return result;
        }

        /// <summary>
        /// Builds a date string from a month, day, and year.
        /// </summary>
        public static string ToDateString(string month, string day, string year)
        {
            string date = String.Empty;

            date = month + "/" + day + "/" + year;

            return date;
        }


        public static class NumberParser
        {
            /// <summary>
            /// Returns string value representation of money.
            /// </summary>
            public static string ToMoney(decimal value)
            {
                string money = "0.00";

                try
                {
                    money = value.ToString("C");
                }
                catch
                { }

                return money;
            }

            /// <summary>
            /// Converts string/number value of a telephone number into the format (XXX) XXX-XXXX.
            /// Return's empty string if unable to convert.
            /// </summary>
            public static string ToPhoneNumber(string value)
            {
                try
                {
                    value = new Regex(@"\D").Replace(value, String.Empty);
                    value = new Regex(@"\.").Replace(value, String.Empty);
                    value = new Regex(@"\-").Replace(value, String.Empty);
                    value = new Regex(@"\(").Replace(value, String.Empty);
                    value = new Regex(@"\)").Replace(value, String.Empty);

                    value = value.TrimStart('1');


                    if (value.Length == 7)
                        return Convert.ToInt64(value).ToString("###-####");
                    if (value.Length == 10)
                        return Convert.ToInt64(value).ToString("###-###-####");
                    if (value.Length > 10)
                        return Convert.ToInt64(value)
                            .ToString("###-###-#### " + new String('#', (value.Length - 10)));
                }
                catch
                {
                    value = String.Empty;
                }


                return value;

            }

        }


        /// <summary>
        /// Removes all non numeric characters from a string.  E.g. "36-789-AB-C-000" = "36789000"
        /// </summary>
        /// <param name="str">String to format.</param>
        /// <returns>String</returns>
        public static string RemoveNonNumericChars(string str)
        {
            string result = String.Empty;

            try
            {
                result = Regex.Replace(str, "[^0-9]", "");
            }
            catch
            {
                result = str;
            }

            return result;
        }

        public static class DateParser
        {
            /// <summary>
            /// Gets date string from date/time string.
            /// Returns an empty string if it cannot parse the date.
            /// </summary>
            public static string Date(string datetime)
            {
                string value = String.Empty;

                try
                {
                    value = Convert.ToDateTime(datetime).ToShortDateString();
                }
                catch
                {

                }

                return value;
            }

            /// <summary>
            /// Returns a date string from a date text string.
            /// Returns an empty string if it cannot parse the date.
            /// </summary>
            public static string TextToDate(string datetime)
            {
                string date = String.Empty;
                bool isConverted = false;

                DateTime outDate;

                try
                {
                    if (DateTime.TryParse(datetime, out outDate))
                    {
                        isConverted = true;
                        date = outDate.ToShortDateString();
                    }

                    if (!isConverted)
                    {
                        if (datetime.Length == 8)
                        {
                            date = Convert.ToInt32(datetime.Substring(4, 2)).ToString() + "-" + Convert.ToInt32(datetime.Substring(6, 2)).ToString() + "-" + Convert.ToInt32(datetime.Substring(0, 4)).ToString();

                            if (DateTime.TryParse(date, out outDate))
                            {
                                isConverted = true;
                                date = outDate.ToShortDateString();
                            }

                        }
                    }
                }
                catch
                {

                }

                return date;
            }

            /// <summary>
            /// Gets month from date/time string.
            /// Returns an empty string if it cannot parse the date.
            /// </summary>
            public static string Month(string datetime)
            {
                string value = String.Empty;

                try
                {
                    value = Convert.ToDateTime(datetime).Month.ToString();
                }
                catch
                {

                }

                return value;
            }

            /// <summary>
            /// Gets day from date/time string.
            /// Returns an empty string if it cannot parse the date.
            /// </summary>
            public static string Day(string datetime)
            {
                string value = String.Empty;

                try
                {
                    value = Convert.ToDateTime(datetime).Day.ToString();
                }
                catch
                {

                }

                return value;
            }

            /// <summary>
            /// Gets year from date/time string.
            /// Returns an empty string if it cannot parse the date.
            /// </summary>
            public static string Year(string datetime)
            {
                string value = String.Empty;

                try
                {
                    value = Convert.ToDateTime(datetime).Year.ToString();
                }
                catch
                {

                }

                return value;
            }


        }

    }








}