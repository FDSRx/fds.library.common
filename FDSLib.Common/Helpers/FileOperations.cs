﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace FDSLib.Common.Helpers
{
    /// <summary>
    /// Provides a set of file reading and writing
    /// </summary>
    public static class FileOperations
    {

        /// <summary>
        /// Creates or overwrites a file in the specified path.
        /// </summary>
        /// <param name="path">The path and name of the file to create.</param>
        public static void Create(string path)
        {
            if (!String.IsNullOrEmpty(path) && !File.Exists(path))
                File.Create(path);

            //Thread.Sleep(1000);

            //GC.Collect();
        }

        /// <summary>
        /// Creates or overwrites a file in the specified path.
        /// </summary>
        /// <param name="path">The path and name of the file to create.</param>
        /// <param name="bufferSize">The number of bytes buffered for reads and writes to the file.</param>
        public static void Create(string path, int bufferSize)
        {
            if (!String.IsNullOrEmpty(path) && !File.Exists(path))
                File.Create(path, bufferSize);

            //Thread.Sleep(1000);

            //GC.Collect();
        }

        /// <summary>
        /// Creates or overwrites a file in the specified path. Specifying a buffer size and a System.IO.FileOptions value that 
        /// describes how to create or overwrite the file.
        /// </summary>
        /// <param name="path">The path and name of the file to create.</param>
        /// <param name="bufferSize">The number of bytes buffered for reads and writes to the file.</param>
        /// <param name="options">One of the System.IO.FileOptions values that describes how to create or overwrite the file.</param>
        public static void Create(string path, int bufferSize, FileOptions options)
        {
            if (!String.IsNullOrEmpty(path) && !File.Exists(path))
                File.Create(path, bufferSize, options);

            //Thread.Sleep(1000);

            //GC.Collect();
        }

        /// <summary>
        /// Creates or overwrites a file in the specified path. Specifying a buffer size and a System.IO.FileOptions value that 
        /// describes how to create or overwrite the file.
        /// </summary>
        /// <param name="path">The path and name of the file to create.</param>
        /// <param name="bufferSize">The number of bytes buffered for reads and writes to the file.</param>
        /// <param name="options">One of the System.IO.FileOptions values that describes how to create or overwrite the file.</param>
        /// <param name="fileSecurity">One of the System.Security.AccessControl.FileSecurity values that determines 
        /// the acccess control and audit security for the file.</param>
        public static void Create(string path, int bufferSize, FileOptions options, System.Security.AccessControl.FileSecurity fileSecurity)
        {
            if (!String.IsNullOrEmpty(path) && !File.Exists(path))
                File.Create(path, bufferSize, options, fileSecurity);

            //Thread.Sleep(1000);

            //GC.Collect();
        }

        /// <summary>
        /// Returns the entire contents of a file
        /// </summary>
        /// <param name="sourceFile">Full file path and name</param>
        /// <returns>File contents</returns>
        public static string Read(string sourceFile)
        {
            return Read(sourceFile, 0, 0);
        }

        /// <summary>
        /// Returns the contents of a file, limited by the start and length
        /// </summary>
        /// <param name="sourceFile">Full file path and name</param>
        /// <param name="start">Which position to begin reading (0 will start at beginning of file)</param>
        /// <param name="length">The maximum number of bytes to read (0 will read to end of file)</param>
        /// <returns>File contents</returns>
        public static string Read(string sourceFile, int start, int length)
        {
            string contents;

            if (string.IsNullOrEmpty(sourceFile) || !File.Exists(sourceFile))
            {
                throw new FileNotFoundException(
                    string.Format("The specified file {0} could not be found. Operation aborting.", sourceFile)
                    );
            }

            if (start < 0)
            {
                throw new ArgumentException("Parameter cannot be less than zero. Operation aborting.", "start");
            }

            if (length < 0)
            {
                throw new ArgumentException("Parameter cannot be less than zero. Operation aborting.", "length");
            }

            using (StreamReader streamReader = new StreamReader(sourceFile))
            {

                if (start > 0)
                {
                    if (start > streamReader.BaseStream.Length)
                        throw new IndexOutOfRangeException("Parameter 'start' is greater than file length. Operation aborting.");
                }

                if (length == 0 || length > ((int)streamReader.BaseStream.Length - start))
                {
                    length = (int)streamReader.BaseStream.Length - start;
                }

                char[] buffer = new char[length];

                streamReader.BaseStream.Seek(start, SeekOrigin.Begin);
                streamReader.Read(buffer, 0, buffer.Length);
                //streamReader.Close();

                contents = new string(buffer);
            }

            return contents;

        }

        /// <summary>
        /// Returns the contents of a file, limited by the start and length
        /// </summary>
        /// <param name="sourceFile">Full file path and name</param>
        /// <returns>File contents</returns>
        public static Stream ReadStream(string sourceFile)
        {
            return ReadStream(sourceFile, 0, 0);
        }

        /// <summary>
        /// Returns the contents of a file, limited by the start and length
        /// </summary>
        /// <param name="sourceFile">Full file path and name</param>
        /// <param name="start">Which position to begin reading (0 will start at beginning of file)</param>
        /// <param name="length">The maximum number of bytes to read (0 will read to end of file)</param>
        /// <returns>File contents</returns>
        public static Stream ReadStream(string sourceFile, int start, int length)
        {
            return new MemoryStream(ReadBytes(sourceFile, start, length));
        }

        /// <summary>
        /// Returns the contents of a file
        /// </summary>
        /// <param name="sourceFile">Full file path and name</param>
        /// <returns>File contents</returns>
        public static byte[] ReadBytes(string sourceFile)
        {
            return ReadBytes(sourceFile, 0, 0);
        }

        /// <summary>
        /// Returns the contents of a file, limited by the start and length
        /// </summary>
        /// <param name="sourceFile">Full file path and name</param>
        /// <param name="start">Which position to begin reading (0 will start at beginning of file)</param>
        /// <param name="length">The maximum number of bytes to read (0 will read to end of file)</param>
        /// <returns>File contents</returns>
        public static byte[] ReadBytes(string sourceFile, int start, int length)
        {
            byte[] buffer;

            if (string.IsNullOrEmpty(sourceFile) || !File.Exists(sourceFile))
            {
                throw new FileNotFoundException(
                    string.Format("The specified file {0} could not be found. Operation aborting.", sourceFile)
                    );
            }

            if (start < 0)
            {
                throw new ArgumentException("Parameter cannot be less than zero. Operation aborting.", "start");
            }

            if (length < 0)
            {
                throw new ArgumentException("Parameter cannot be less than zero. Operation aborting.", "length");
            }

            using (StreamReader streamReader = new StreamReader(sourceFile))
            {

                if (start > 0)
                {
                    if (start > streamReader.BaseStream.Length)
                        throw new IndexOutOfRangeException("Parameter 'start' is greater than file length. Operation aborting.");
                }

                if (length == 0 || length > ((int)streamReader.BaseStream.Length - start))
                {
                    length = (int)streamReader.BaseStream.Length - start;
                }

                buffer = new byte[length];

                streamReader.BaseStream.Seek(start, SeekOrigin.Begin);
                streamReader.BaseStream.Read(buffer, 0, buffer.Length);
                //streamReader.Close();
            }

            return buffer;

        }

        /// <summary>
        /// Returns the lines if a file
        /// </summary>
        /// <param name="sourceFile">Full file path and name</param>
        /// <returns></returns>
        public static string[] ReadLines(string sourceFile)
        {
            List<string> list = new List<string>();

            if (File.Exists(sourceFile))
            {
                FileStream fileStream = null;

                try
                {
                    fileStream = new FileStream(sourceFile, FileMode.Open, FileAccess.Read);

                    using (StreamReader streamReader = new StreamReader(fileStream))
                    {
                        fileStream = null;

                        while (streamReader.Peek() != -1)
                        {
                            list.Add(streamReader.ReadLine());
                        }
                    }
                }
                finally
                {
                    if (fileStream != null)
                    {
                        fileStream.Dispose();
                    }
                }
            }

            return list.ToArray();
        }

        /// <summary>
        /// Writes to a file (overwrites if exists)
        /// </summary>
        /// <param name="destinationFile">Full file path and name</param>
        /// <param name="contents">Content to write to file</param>
        public static void Write(string destinationFile, string contents)
        {
            Write(destinationFile, contents, true);
        }

        /// <summary>
        /// Writes to a file 
        /// </summary>
        /// <param name="destinationFile">Full file path and name</param>
        /// <param name="contents">Content to write to file</param>
        /// <param name="overwrite">Overwrites if true, appends if false</param>
        public static void Write(string destinationFile, string contents, bool overwrite)
        {
            Write(destinationFile, contents.ToCharArray(), overwrite);
        }

        /// <summary>
        /// Writes to a file 
        /// </summary>
        /// <param name="destinationFile">Full file path and name</param>
        /// <param name="contents">Content to write to file</param>
        /// <param name="overwrite">Overwrites if true, appends if false</param>
        /// <param name="binary">Whether to use binary write approach</param>
        public static void Write(string destinationFile, Stream contents, bool overwrite, bool binary)
        {
            byte[] byteArray = new byte[(int)contents.Length];
            contents.Seek(0, SeekOrigin.Begin);
            contents.Read(byteArray, 0, byteArray.Length);

            if (binary)
            {
                Write(destinationFile, byteArray, overwrite);
            }
            else
            {
                char[] charArray = System.Text.Encoding.Default.GetChars(byteArray);
                Write(destinationFile, charArray, overwrite);
            }
        }

        /// <summary>
        /// Writes to a file 
        /// </summary>
        /// <param name="destinationFile">Full file path and name</param>
        /// <param name="contents">Content to write to file</param>
        /// <param name="overwrite">Overwrites if true, appends if false</param>
        public static void Write(string destinationFile, byte[] contents, bool overwrite)
        {
            FileMode fileMode = FileMode.OpenOrCreate;
            FileAccess fileAccess = FileAccess.ReadWrite;


            if (File.Exists(destinationFile) && !overwrite)
            {
                File.SetAttributes(destinationFile, FileAttributes.Normal);
                fileMode = FileMode.Append;
                fileAccess = FileAccess.Write;
            }

            FileStream fileStream = null;

            try
            {
                fileStream = new FileStream(destinationFile, fileMode, fileAccess);
                using (BinaryWriter binaryWriter = new BinaryWriter(fileStream))
                {
                    fileStream = null;
                    binaryWriter.Write(contents);
                }
            }
            finally
            {
                if (fileStream != null)
                {
                    fileStream.Dispose();
                }
            }
        }

        /// <summary>
        /// Writes to a file 
        /// </summary>
        /// <param name="destinationFile">Full file path and name</param>
        /// <param name="contents">Content to write to file</param>
        /// <param name="overwrite">Overwrites if true, appends if false</param>
        public static void Write(string destinationFile, char[] contents, bool overwrite)
        {
            FileMode fileMode = FileMode.OpenOrCreate;
            FileAccess fileAccess = FileAccess.ReadWrite;

            if (File.Exists(destinationFile) && !overwrite)
            {
                File.SetAttributes(destinationFile, FileAttributes.Normal);
                fileMode = FileMode.Append;
                fileAccess = FileAccess.Write;
            }

            FileStream fileStream = null;

            try
            {
                fileStream = new FileStream(destinationFile, fileMode, fileAccess);
                using (StreamWriter streamWriter = new StreamWriter(fileStream))
                {
                    fileStream = null;
                    streamWriter.Write(contents);
                }
            }
            finally
            {
                if (fileStream != null)
                {
                    fileStream.Dispose();
                }
            }
        }

        /// <summary>
        /// Gets all subdirectories and files within a directory
        /// </summary>
        /// <param name="sourceDirectory">Directory containing files and subdirectories</param>
        /// <param name="fileList">File list dictionary. Key is full file path and name, Value is file name and path relative to the base directory</param>
        /// <param name="baseDirectory">Base directory (empty for first-level directory, used in recursion or as a prefix)</param>
        public static void GetFilesAndDirectories(string sourceDirectory, Dictionary<string, string> fileList, string baseDirectory)
        {
            foreach (string file in Directory.GetFiles(sourceDirectory))
            {
                string entryName = string.Concat(baseDirectory, @"\", Path.GetFileName(file));
                fileList.Add(file, entryName);
            }

            foreach (string subDirectory in Directory.GetDirectories(sourceDirectory))
            {
                // This wouldn't work if the subDirectory had a '\' at the end
                string subDirectoryName = Path.GetFileName(subDirectory);

                GetFilesAndDirectories(subDirectory, fileList,
                    string.Concat(baseDirectory, @"\", subDirectoryName));
            }
        }

        /// <summary>
        /// Returns the names of files (including their paths) in the specified directory.
        /// </summary>
        /// <param name="directory">The directory to search.</param>
        /// <param name="searchPattern">The search string to match against the names of files in a path. The parameter cannot end in two periods ("..")
        /// or contain two periods ("..") followed by System.IO.Path.DirectorySeparatorChar or System.IO.Path.AltDirectorySeparatorChar, nor can it contain
        /// any of the characters in System.IO.Path.InvalidPathChars.</param>
        /// <param name="searchOption">One of the System.IO.SearchOption values that specifies whether the search operation should include all 
        /// subdirectories or only the current directory.</param>
        /// <returns></returns>
        public static string[] GetFiles(string directory, string searchPattern, SearchOption searchOption)
        {
            string[] searchPatterns = searchPattern.Split('|');

            List<string> files = new List<string>();

            foreach (string pattern in searchPatterns)
            {
                files.AddRange(Directory.GetFiles(directory, pattern, searchOption));
            }
            files.Sort();
            return files.ToArray();
        }

        /// <summary>
        /// Returns the names of files (including their paths) in the specified directory.
        /// </summary>
        /// <param name="directory">The directory to search.</param>
        /// <param name="searchPattern">The search string to match against the names of files in a path. The parameter cannot end in two periods ("..")
        /// or contain two periods ("..") followed by System.IO.Path.DirectorySeparatorChar or System.IO.Path.AltDirectorySeparatorChar, nor can it contain
        /// any of the characters in System.IO.Path.InvalidPathChars.</param>
        /// <param name="searchOption">One of the System.IO.SearchOption values that specifies whether the search operation should include all 
        /// subdirectories or only the current directory.</param>
        /// <param name="batchSize"></param>
        /// <returns></returns>
        public static string[] GetFiles(string directory, string searchPattern, SearchOption searchOption, int batchSize)
        {
            List<string> files = new List<string>();

            if (Directory.Exists(directory))
            {
                string[] allFiles = Directory.GetFiles(directory, searchPattern, searchOption);

                if (allFiles.Length > 0)
                {
                    for (int i = 1; i != allFiles.Length; i++)
                    {
                        files.Add(allFiles[i]);

                        if (i == batchSize)
                            break;
                    }
                    files.Sort();
                }

                return files.ToArray();
            }

            return null;
        }

        /// <summary>
        /// Copies an existing file to a new file.  Overwritting an existing file is allowed.
        /// </summary>
        /// <param name="source">The file to copy.</param>
        /// <param name="destination">The name of the destination file.  This cannot be a directory or an existing file.</param>
        /// <param name="overwrite">True if the destination file can be overwritten; otherwise, false.</param>
        public static void CopyFile(string source, string destination, bool overwrite)
        {
            string path = Path.GetDirectoryName(destination);

            if (string.IsNullOrEmpty(path))
            {
                return;
            }

            if (Directory.Exists(path))
            {
                if (File.Exists(source))
                {
                    File.Copy(source, destination, overwrite);
                }
            }
        }

        /// <summary>
        /// Deletes the specified file.
        /// </summary>
        /// <param name="sourceFilePath">The name of the file to be deleted. Wildcard characters are not supported.</param>
        public static void DeleteFile(string sourceFilePath)
        {
            File.Delete(sourceFilePath);
        }

        /// <summary>
        /// Indicates whether the directory contains any files.
        /// </summary>
        /// <param name="sourceDirectory">Path of directory.</param>
        /// <returns></returns>
        public static bool? IsDirectoryEmpty(string sourceDirectory)
        {
            if (Directory.Exists(sourceDirectory))
            {
                string[] s = Directory.GetFiles(sourceDirectory);
                if (s.Length > 0)
                {
                    return false;
                }
                return true;
            }
            return null;
        }

        /// <summary>
        /// Deletes the specified directory.
        /// </summary>
        /// <param name="sourceDirectory">Path of directory.</param>
        public static void DeleteDirectory(string sourceDirectory)
        {
            Directory.Delete(sourceDirectory, true);
        }

        /// <summary>
        /// Simple file name formatter.
        /// </summary>
        /// <param name="name">Name of file.</param>
        /// <param name="extension">Extension of file.</param>
        /// <returns></returns>
        public static string GetFileName(string name, string extension)
        {
            StringBuilder nameBuilder = new StringBuilder();

            if (!string.IsNullOrEmpty(name))
            {
                nameBuilder.Append(name);
            }

            if (nameBuilder.Length == 0)
                throw new ArgumentException("No information provided for file name");

            if (!string.IsNullOrEmpty(extension))
            {
                nameBuilder.Append(".");
                nameBuilder.Append(extension);
            }

            return nameBuilder.ToString();
        }

        /// <summary>
        /// Returns a standard file name, using an underscore as the delimiter
        /// </summary>
        /// <param name="prefix">File prefix</param>
        /// <param name="suffix">File suffix</param>
        /// <param name="extension">File extention (e.g. "txt")</param>
        /// <param name="includeDate">Include date, in format yyyyMMdd</param>
        /// <param name="includeTime">Include time, in format hhmmss</param>
        /// <param name="appendGuid">Append GUID to end of file name</param>
        /// <returns>Computed file name</returns>
        public static string GetFileName(
            string prefix,
            string suffix,
            string extension,
            bool includeDate,
            bool includeTime,
            bool appendGuid)
        {
            return GetFileName(prefix, suffix, extension, "_", includeDate, includeTime, appendGuid);
        }

        /// <summary>
        /// Returns a standard file name
        /// </summary>
        /// <param name="prefix">File prefix</param>
        /// <param name="suffix">File suffix</param>
        /// <param name="extension">File extention (e.g. "txt")</param>
        /// <param name="delimiter">String used to separate file name parts</param>
        /// <param name="includeDate">Include date, in format yyyyMMdd</param>
        /// <param name="includeTime">Include time, in format hhmmss</param>
        /// <param name="appendGuid">Append GUID to end of file name</param>
        /// <returns>Computed file name</returns>
        public static string GetFileName(
            string prefix,
            string suffix,
            string extension,
            string delimiter,
            bool includeDate,
            bool includeTime,
            bool appendGuid)
        {
            StringBuilder nameBuilder = new StringBuilder();
            DateTime now = DateTime.Now;

            if (!string.IsNullOrEmpty(prefix))
            {
                nameBuilder.Append(prefix);
                nameBuilder.Append(delimiter);
            }

            if (includeDate)
            {
                nameBuilder.Append(now.Year.ToString("D4"));
                nameBuilder.Append(now.Month.ToString("D2"));
                nameBuilder.Append(now.Day.ToString("D2"));
                nameBuilder.Append(delimiter);
            }

            if (includeTime)
            {
                nameBuilder.Append(now.Hour.ToString("D2"));
                nameBuilder.Append(now.Minute.ToString("D2"));
                nameBuilder.Append(now.Second.ToString("D2"));
                nameBuilder.Append(delimiter);
            }

            if (!string.IsNullOrEmpty(suffix))
            {
                nameBuilder.Append(suffix);
            }

            if (appendGuid)
            {
                if (nameBuilder.Length > 0)
                    nameBuilder.Append(delimiter);
                nameBuilder.Append(Guid.NewGuid());
            }

            if (nameBuilder.Length == 0)
                throw new ArgumentException("No information provided for file name");

            if (!string.IsNullOrEmpty(extension))
            {
                nameBuilder.Append(".");
                nameBuilder.Append(extension);
            }

            return nameBuilder.ToString();
        }


    }
}
