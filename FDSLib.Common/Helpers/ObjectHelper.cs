﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace FDSLib.Common.Helpers
{
    /// <summary>
    /// Various object helper methods.
    /// </summary>
    public static class ObjectHelper
    {
        /// <summary>
        /// Converts the object to the specified type.
        /// </summary>
        /// <typeparam name="T">Type to convert.</typeparam>
        /// <param name="obj">Object to convert to specified type.</param>
        /// <returns></returns>
        public static T ConvertToType<T>(object obj)
        {
            var surfaceType = typeof(T);
            var underlyingType = Nullable.GetUnderlyingType(surfaceType);

            if (underlyingType != null)
            {
                if (obj == null)
                    return default(T);

                return (T)Convert.ChangeType(obj, underlyingType);
            }

            return (T)Convert.ChangeType(obj, surfaceType);
        }

        /// <summary>
        /// Creates a deep copy of the provided object (of type T).
        /// </summary>
        /// <typeparam name="T">Object (of type T).</typeparam>
        /// <param name="obj">The object to be deep copied.</param>
        /// <returns></returns>
        public static T DeepyCopy<T>(T obj)
        {
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();

            bf.Serialize(ms, obj);
            ms.Flush();
            ms.Position = 0;

            var clone = (T)bf.Deserialize(ms);
            ms.Close();

            return clone;
        }

        /// <summary>
        /// Gets the default value for the provided type.
        /// </summary>
        /// <typeparam name="T">Object type.</typeparam>
        /// <returns></returns>
        public static T GetDefault<T>()
        {
            // We want an Func<T> which returns the default.
            // Create that expression here.
            Expression<Func<T>> e = Expression.Lambda<Func<T>>(
                // The default value, always get what the *code* tells us.
                Expression.Default(typeof(T))
            );

            // Compile and return the value.
            return e.Compile()();
        }

        /// <summary>
        /// Gets the default value for the provided type.
        /// </summary>
        /// <param name="type">Objeect type.</param>
        /// <returns></returns>
        public static object GetDefault(Type type)
        {
            // Validate parameters.
            if (type == null) throw new ArgumentNullException("type");

            // We want an Func<object> which returns the default.
            // Create that expression here.
            Expression<Func<object>> e = Expression.Lambda<Func<object>>(
                // Have to convert to object.
                Expression.Convert(
                // The default value, always get what the *code* tells us.
                    Expression.Default(type), typeof(object)
                )
            );

            // Compile and return the value.
            return e.Compile()();
        }

        /// <summary>
        /// Encodes the object into a base64 string.
        /// </summary>
        /// <param name="obj">Object to encode.</param>
        /// <returns></returns>
        public static string EncodeToBase64String(object obj)
        {
            using (var ms = new MemoryStream())
            {
                new BinaryFormatter().Serialize(ms, obj);
                return Convert.ToBase64String(ms.ToArray());
            }
        }

        /// <summary>
        /// Decodes the object from a base64 string.
        /// </summary>
        /// <param name="base64String">The base64 string to decode.</param>
        /// <returns></returns>
        public static object DecodeFromBase64String(string base64String)
        {
            byte[] bytes = Convert.FromBase64String(base64String);
            using (var ms = new MemoryStream(bytes, 0, bytes.Length))
            {
                ms.Write(bytes, 0, bytes.Length);
                ms.Position = 0;
                return new BinaryFormatter().Deserialize(ms);
            }
        }



    }




}
