﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace FDSLib.Common.Helpers
{
    public class WebConfigHelper
    {
        /// <summary>
        /// Returns the connection string for the provided key.  Returns null if unable to locate the key.
        /// </summary>
        /// <param name="key">Connection string key name.</param>
        /// <returns></returns>
        public static string GetConnectionString(string key)
        {
            return GetConnectionString(key, null);
        }

        /// <summary>
        /// Returns the connection string for the provided key.  Returns null if unable to locate the key.
        /// </summary>
        /// <param name="key">Connection string key name.</param>
        /// <param name="emptyKeyValue">Value to be used in place of a null connection string.</param>
        /// <returns></returns>
        public static string GetConnectionString(string key, string emptyKeyValue)
        {
            string value;

            try
            {
                value = ConfigurationManager.ConnectionStrings[key] != null ? ConfigurationManager.ConnectionStrings[key].ConnectionString : null;

                value = !String.IsNullOrEmpty(value) ? value : emptyKeyValue;

            }
            catch
            {
                value = emptyKeyValue;
            }

            return value;
        }

        /// <summary>
        /// Returns the value for the key. Returns null if unable to locate key.
        /// </summary>
        /// <param name="key">Setting key name.</param>
        public static string GetAppSetting(string key)
        {
            return GetAppSetting(key, null);
        }

        /// <summary>
        /// Returns the value for the key.  Returns null if an emptyKeyValue is not supplied.
        /// </summary>
        /// <param name="key">Setting key name.</param>
        /// <param name="emptyKeyValue">Optional return value if the key is not found in the collection.</param>
        /// <returns></returns>
        public static string GetAppSetting(string key, string emptyKeyValue)
        {
            string value;

            try
            {
                value = ConfigurationManager.AppSettings[key];

                value = !String.IsNullOrEmpty(value) ? value : emptyKeyValue;
            }
            catch
            {
                value = emptyKeyValue;
            }

            return value;
        }

        /// <summary>
        /// Returns the value for the key. Returns null if unable to locate key.
        /// </summary>
        /// <param name="key">Setting key name.</param>
        public static T GetAppSetting<T>(string key)
        {
            var t = GetAppSetting(key);

            return ObjectHelper.ConvertToType<T>(t);
        }

        /// <summary>
        /// Returns the value for the key. Returns null if unable to locate key.
        /// </summary>
        /// <param name="key">Setting key name.</param>
        /// <param name="emptyKeyValue">Optional return value if the key is not found in the collection.</param>
        /// <returns></returns>
        public static T GetAppSetting<T>(string key, T emptyKeyValue)
        {
            var t = GetAppSetting(key);

            return t == null ? emptyKeyValue : ObjectHelper.ConvertToType<T>(t);
        }

        /// <summary>
        /// Returns the value for the key. Returns null if unable to locate key.
        /// </summary>
        /// <param name="key">Setting key name.</param>
        public static T GetAppSettingOrDefault<T>(string key)
        {
            var t = GetAppSetting(key);

            return t == null ? default(T) : ObjectHelper.ConvertToType<T>(t);
        }

        /// <summary>
        /// Returns the value for the key. Returns null if unable to locate key.
        /// </summary>
        /// <param name="key">Setting key name.</param>
        /// <param name="emptyKeyValue">Optional return value if the key is not found in the collection.</param>
        /// <returns></returns>
        public static T GetAppSettingOrDefault<T>(string key, T emptyKeyValue)
        {
            var t = GetAppSetting(key);

            return (t == null) ? emptyKeyValue : ObjectHelper.ConvertToType<T>(t);
        }

        /// <summary>
        /// Returns the value for the key. Returns null if unable to locate key.
        /// </summary>
        /// <param name="key">Setting key name.</param>
        public static T? GetAppSettingOrNull<T>(string key) where T : struct
        {
            var t = GetAppSetting(key);

            return (t == null) ? (T?)null : ObjectHelper.ConvertToType<T>(t);
        }

        /// <summary>
        /// Returns the value for the key. Returns null if unable to locate key.
        /// </summary>
        /// <param name="key">Setting key name.</param>
        /// <param name="emptyKeyValue">Optional return value if the key is not found in the collection.</param>
        /// <returns></returns>
        public static T? GetAppSettingOrNull<T>(string key, T? emptyKeyValue) where T : struct
        {
            var t = GetAppSetting(key);

            return (t == null) ? emptyKeyValue : ObjectHelper.ConvertToType<T>(t);
        }

   


    }
}