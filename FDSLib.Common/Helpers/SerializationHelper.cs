﻿using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.Script.Serialization;
using System.Xml;
using System.Xml.Serialization;
using FDSLib.Common.Formatters;
using FDSLib.Common.Xml.Serialization;


namespace FDSLib.Common.Helpers
{
    /// <summary>
    /// Provides static methods for interfacing with a variety of serializers. 
    /// </summary>
    public static class SerializationHelper
    {

        private static IXmlSerializationSettings _xmlSerializationSettings = new XmlSerializationSettings();

        #region Registration Methods

        /// <summary>
        /// Registers serializations settings for the various serializations methods.
        /// </summary>
        /// <param name="xmlSerializationSettings">IXmlSerializationSettings object.</param>
        public static void Register(IXmlSerializationSettings xmlSerializationSettings)
        {
            if (xmlSerializationSettings == null)
            {
                throw new ArgumentNullException("xmlSerializationSettings");
            }

            _xmlSerializationSettings = xmlSerializationSettings;
        }

        #endregion

        #region Xml Methods

        #region Serialize Methods

        /// <summary>
        /// Serialize an object into an xml string.
        /// </summary>
        /// <param name="source">Source object.</param>
        /// <param name="withNamespace">Indicates whether the xml string should contain a namespace.</param>
        /// <returns>An xml string.</returns>
        public static string SerializeToXml(object source, bool withNamespace)
        {
            var settings = new XmlSerializationSettings();

            if (!withNamespace)
            {
                settings.ReplaceContentRules.Add("<?xml version=\"1.0\"?>", "");
                settings.ReplaceContentRules.Add(Environment.NewLine, "");
            }

            return SerializeToXml(source, settings);
        }

        /// <summary>
        /// Serialize an object into an xml string.
        /// </summary>
        /// <param name="source">Source object.</param>
        /// <param name="xmlNamespace1">Namespace.</param>
        /// <param name="xmlNamespace2">Namespace.</param>
        /// <returns></returns>
        public static string SerializeToXml(object source, string xmlNamespace1, string xmlNamespace2)
        {
            var settings = new XmlSerializationSettings();

            settings.Namespaces.Add("i", xmlNamespace1);
            settings.Namespaces.Add(String.Empty, xmlNamespace2);

            settings.ReplaceContentRules.Add("<?xml version=\"1.0\"?>", "");
            settings.ReplaceContentRules.Add("q1:", "");
            settings.ReplaceContentRules.Add("q1", "");
            settings.ReplaceContentRules.Add(Environment.NewLine, "");

            return SerializeToXml(source, settings);
        }

        /// <summary>
        /// Serialize an object into an xml string.
        /// </summary>
        /// <param name="source">Source object.</param>
        /// <returns></returns>
        public static string SerializeToXml(object source)
        {
            return SerializeToXml(source, null);
        }

        /// <summary>
        /// Serialize an object into an xml string.
        /// </summary>
        /// <param name="source">Source object to serialize.</param>
        /// <param name="settings">IXmlSerializationSettings object.</param>
        /// <returns></returns>
        public static string SerializeToXml(object source, IXmlSerializationSettings settings)
        {
            settings = settings ?? _xmlSerializationSettings;

            var xmlns = new XmlSerializerNamespaces();

            var writerSettings = settings.XmlWriterSettings ?? new XmlWriterSettings();

            string result;

            //Apply namespaces (if applicable) to the namespace collection.
            if (settings.Namespaces != null && settings.Namespaces.Any())
            {
                foreach (var item in settings.Namespaces)
                {
                    xmlns.Add(item.Key, item.Value);
                }
            }

            using (var stringWriter = new XmlStringWriter())
            {
                using (var xmlWriter = XmlWriter.Create(stringWriter, writerSettings))
                {
                    XmlSerializer serializer = new XmlSerializer(source.GetType());
                    serializer.Serialize(xmlWriter, source, xmlns);

                    result = stringWriter.ToString();

                    //Apply replace content rules to the final xml result.
                    if (settings.ReplaceContentRules != null && settings.ReplaceContentRules.Any())
                    {
                        foreach (var item in settings.ReplaceContentRules)
                        {
                            result = result.Replace(item.Key, item.Value);
                        }
                    }
                }
            }

            return result;
        }

        #endregion

        #region Deserialize Methods

        /// <summary>
        /// Deserialize an xml string into an object.
        /// </summary>
        /// <param name="xml">The xml string.</param>
        /// <param name="type">The type.</param>
        /// <returns>An object.</returns>
        public static object DeserializeXml(string xml, Type type)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                UTF8Encoding encoder = new UTF8Encoding();
                byte[] loadString = encoder.GetBytes(xml);
                stream.Write(loadString, 0, loadString.Length);
                stream.Seek(0, SeekOrigin.Begin);

                using (XmlTextReader reader = new XmlTextReader(stream))
                {
                    XmlSerializer serializer = new XmlSerializer(type);
                    return serializer.Deserialize(reader);
                }
            }
        }

        /// <summary>
        /// Deserialize an xml string into an object. If xml string is unable to be deserialized into the object then its default value will be returned.
        /// </summary>
        /// <param name="xml">The xml string.</param>
        /// <param name="type">The object type.</param>
        /// <returns></returns>
        public static object DeserializeXmlOrDefault(string xml, Type type)
        {
            try
            {
                return DeserializeXml(xml, type);
            }
            catch
            {
                return ObjectHelper.GetDefault(type);
            }
        }

        /// <summary>
        /// Deserialize an xml string into an object(of T).
        /// </summary>
        /// <param name="xml">The xml string.</param>
        /// <returns>The implied object type.</returns>
        public static T DeserializeXml<T>(string xml)
        {
            return DeserializeXml<T>(xml, null);
        }

        /// <summary>
        /// Deserialize an xml string into an object(of T).
        /// </summary>
        /// <param name="xml">The xml string.</param>
        /// <param name="root">A System.Xml.Serialization.XmlRootAttribute that represents the XML root element.</param>
        /// <returns>The implied object type.</returns>
        public static T DeserializeXml<T>(string xml, XmlRootAttribute root)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                UTF8Encoding encoder = new UTF8Encoding();
                byte[] loadString = encoder.GetBytes(xml);
                stream.Write(loadString, 0, loadString.Length);
                stream.Seek(0, SeekOrigin.Begin);

                using (XmlTextReader reader = new XmlTextReader(stream))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(T), root);
                    return (T)serializer.Deserialize(reader);
                }
            }
        }

        /// <summary>
        /// Deserialize an xml string into an object(Of t). If xml string is unable to be deserialized into the object then its default value will be returned.
        /// </summary>
        /// <param name="xml">The xml string.</param>
        /// <returns></returns>
        public static T DeserializeXmlOrDefault<T>(string xml)
        {
            try
            {
                return DeserializeXml<T>(xml);
            }
            catch
            {
                return ObjectHelper.GetDefault<T>();
            }
        }

        /// <summary>
        /// Deserialize an xml string into an object(Of t). If xml string is unable to be deserialized into the object then a new instantiated object will be returned.
        /// </summary>
        /// <param name="xml">The xml string.</param>
        /// <returns></returns>
        public static T DeserializeXmlOrNew<T>(string xml) where T : new()
        {
            try
            {
                return DeserializeXml<T>(xml);
            }
            catch
            {
                return new T();
            }
        }

        /// <summary>
        /// Deserialize an xml string into an object(Of t). If xml string is unable to be deserialized into the object then its default value will be returned.
        /// </summary>
        /// <param name="xml">The xml string.</param>
        /// <param name="root">A System.Xml.Serialization.XmlRootAttribute that represents the XML root element.</param>
        /// <returns></returns>
        public static T DeserializeXmlOrDefault<T>(string xml, XmlRootAttribute root)
        {
            try
            {
                return DeserializeXml<T>(xml, root);
            }
            catch
            {
                return ObjectHelper.GetDefault<T>();
            }
        }

        #endregion

        #endregion

        #region Binary Methods

        #region Serialize Methods

        /// <summary>
        /// Serializes an object into a binary stream of data.
        /// </summary>
        /// <param name="sourceObject">Source object.</param>
        /// <returns>A string.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        public static string SerializeToBinary(object sourceObject)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter bin = new BinaryFormatter();
                bin.Serialize(stream, sourceObject);

                stream.Seek(0, SeekOrigin.Begin);
                using (MemoryStream output = new MemoryStream())
                {
                    using (DeflateStream deflateStream = new DeflateStream(output, CompressionMode.Compress))
                    {
                        stream.CopyTo(deflateStream);
                        deflateStream.Close();
                        return Convert.ToBase64String(output.ToArray());
                    }
                }
            }
        }

        #endregion

        #region Deserialize Methods

        /// <summary>
        /// Deserialize a binary string into an object.
        /// </summary>
        /// <param name="source">Source object.</param>
        /// <returns>The object.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        public static object DeserializeBinary(string source)
        {
            using (MemoryStream stream = new MemoryStream(Convert.FromBase64String(source)))
            {
                using (DeflateStream deflateStream = new DeflateStream(stream, CompressionMode.Decompress))
                {
                    using (MemoryStream output = new MemoryStream())
                    {
                        deflateStream.CopyTo(output);
                        deflateStream.Close();
                        output.Seek(0, SeekOrigin.Begin);

                        BinaryFormatter bin = new BinaryFormatter();
                        object obj = bin.Deserialize(output);
                        return obj;
                    }
                }
            }
        }


        /// <summary>
        /// Deserialize a binary string into an object.
        /// </summary>
        /// <param name="source">Source object.</param>
        /// <returns>The object.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        public static T DeserializeBinary<T>(string source)
        {
            using (MemoryStream stream = new MemoryStream(Convert.FromBase64String(source)))
            {
                using (DeflateStream deflateStream = new DeflateStream(stream, CompressionMode.Decompress))
                {
                    using (MemoryStream output = new MemoryStream())
                    {
                        deflateStream.CopyTo(output);
                        deflateStream.Close();
                        output.Seek(0, SeekOrigin.Begin);

                        BinaryFormatter bin = new BinaryFormatter();
                        object obj = bin.Deserialize(output);
                        return (T)obj;
                    }
                }
            }
        }

        #endregion

        #endregion

        #region JSON Methods

        #region Serialize Methods

        /// <summary>
        /// Serializes an object to a JSON string.
        /// </summary>
        /// <param name="t">Entity to serialize to a Json object.</param>
        public static string SerializeToJson<T>(T t)
        {
            var jsonSerializer = new JavaScriptSerializer();
            var json = jsonSerializer.Serialize(t);
            return json;
        }

        #endregion

        #region Deserialize Methods

        /// <summary>
        /// Deserializes a JSON string to an object.
        /// </summary>
        /// <param name="jsonString">Serialized Json object string.</param>
        /// <param name="type">Type of object.</param>
        public static object DeserializeJson(string jsonString, Type type)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(type);
            object obj;
            using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString)))
            {
                obj = ser.ReadObject(ms);
            }
            return obj;
        }

        /// <summary>
        /// Deserializes a JSON string to an object. If the string is unable to be deserialized then the default value of the object
        /// is returned.
        /// </summary>
        /// <param name="jsonString">Serialized Json object string.</param>
        /// <param name="type">Type of object.</param>
        public static object DeserializeJsonOrDefault(string jsonString, Type type)
        {
            try
            {
                return DeserializeJson(jsonString, type);
            }
            catch
            {
                return ObjectHelper.GetDefault(type);
            }
        }

        /// <summary>
        /// Deserializes a JSON string to object(T).
        /// </summary>
        /// <param name="jsonString">Serialized Json object string.</param>
        public static T DeserializeJson<T>(string jsonString)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            object obj;
            using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString)))
            {
                obj = ser.ReadObject(ms);
            }
            return (T)obj;
        }

        /// <summary>
        /// Deserializes a JSON string to object(T). If the string is unable to be deserialized then the default value of the object
        /// is returned.
        /// </summary>
        /// <param name="jsonString">Serialized Json object string.</param>
        public static T DeserializeJsonOrDefault<T>(string jsonString)
        {
            try
            {
                return DeserializeJson<T>(jsonString);
            }
            catch
            {
                return ObjectHelper.GetDefault<T>();
            }
        }

        #endregion

        #endregion





    }





}
