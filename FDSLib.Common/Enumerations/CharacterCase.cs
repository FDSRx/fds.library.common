﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Common.Enumerations
{
    /// <summary>
    /// various types of character cases.
    /// </summary>
    public enum CharacterCase
    {
        /// <summary>
        /// Characters are in upper case format (e.g. HELLO WORLD).
        /// </summary>
        Uppercase = 1,

        /// <summary>
        /// Characters are in lower case format (e.g. hello world).
        /// </summary>
        Lowercase = 2,

        /// <summary>
        /// Characters are in a mixed case format (e.g. Hello World).
        /// </summary>
        MixedCase = 3
    }
}
