﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace FDSLib.Common.Enumerations
{
    /// <summary>
    /// Enum attribute that allows a code representation of the Enum.
    /// </summary>
    public class EnumCode : System.Attribute
    {
        private readonly string _value;

        public EnumCode(string value)
        {
            _value = value;
        }

        public string Value
        {
            get { return _value; }
        }

    }

    /// <summary>
    /// Enum extension methods.
    /// </summary>
    public static class CodeEnumExtensions
    {
        /// <summary>
        /// Returns a code representation of the Enum by looking at the EnumCode attribute.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetCode(this Enum value)
        {
            string output = null;

            try
            {
                Type type = value.GetType();

                FieldInfo fi = type.GetField(value.ToString());
                EnumCode[] attrs = fi.GetCustomAttributes(typeof(EnumCode), false) as EnumCode[];

                if (attrs != null && attrs.Length > 0)
                {
                    output = attrs[0].Value;
                }
            }
            catch { }

            return output;
        }
    }
}
