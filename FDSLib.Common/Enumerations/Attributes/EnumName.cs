﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace FDSLib.Common.Enumerations
{
    /// <summary>
    /// Enum attribute that allows a friendly name representation of the Enum.
    /// </summary>
    public class EnumName : System.Attribute
    {
        private readonly string _value;

        public EnumName(string value)
        {
            _value = value;
        }

        public string Value
        {
            get { return _value; }
        }

    }

    /// <summary>
    /// Enum name extension methods.
    /// </summary>
    public static class EnumNameExtensions
    {
        /// <summary>
        /// Returns a frienly name representation of the Enum by looking at the EnumName attribute.
        /// </summary>
        /// <param name="value">The enumeration object.</param>
        /// <returns></returns>
        public static string GetName(this Enum value)
        {
            string output = null;

            try
            {
                Type type = value.GetType();

                FieldInfo fi = type.GetField(value.ToString());
                EnumName[] attrs = fi.GetCustomAttributes(typeof(EnumName), false) as EnumName[];

                if (attrs != null && attrs.Length > 0)
                {
                    output = attrs[0].Value;
                }
            }
            catch { }

            return output;
        }
    }


}
