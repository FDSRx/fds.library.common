﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace FDSLib.Common.Xml.Serialization
{
    public class XmlSerializationSettings : IXmlSerializationSettings
    {

        private const string InstanceName = "XmlSerializationSettings";

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the XmlSerializationSettings object.
        /// </summary>
        public XmlSerializationSettings()
        {
            this.Namespaces = new Dictionary<string, string>();
            this.ReplaceContentRules = new Dictionary<string, string>();
            this.XmlWriterSettings = new XmlWriterSettings();
        }

        #endregion

        #region IXmlSerializationSettings Methods

        /// <summary>
        /// Gets or sets a collection of namespaces.
        /// </summary>
        public IDictionary<string, string> Namespaces { get; set; }

        /// <summary>
        /// A collection of key/value replacement pairs that are applied to the final xml string result. 
        /// </summary>
        public IDictionary<string, string> ReplaceContentRules { get; set; }

        /// <summary>
        /// Specifies a set of features to support on the System.Xml.XmlWriter object created by the Overload:System.Xml.XmlWriter.Create method.
        /// </summary>
        public XmlWriterSettings XmlWriterSettings { get; set; }

        #endregion

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return InstanceName;
        }


    }
}
