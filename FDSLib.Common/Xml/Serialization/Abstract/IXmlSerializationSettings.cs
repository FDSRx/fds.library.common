﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace FDSLib.Common.Xml.Serialization
{
    public interface IXmlSerializationSettings
    {

        /// <summary>
        /// Gets or sets a collection of namespaces.
        /// </summary>
        IDictionary<string, string> Namespaces { get; set; }

        /// <summary>
        /// Gets or sets a collection of key/value replacement pairs that are applied to the final xml string result. 
        /// </summary>
        IDictionary<string, string> ReplaceContentRules { get; set; }

        /// <summary>
        /// Specifies a set of features to support on the System.Xml.XmlWriter object created by the Overload:System.Xml.XmlWriter.Create method.
        /// </summary>
        XmlWriterSettings XmlWriterSettings { get; set; }
    }
}
